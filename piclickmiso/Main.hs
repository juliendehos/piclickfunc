{-# LANGUAGE OverloadedStrings #-}

import           Data.Map (fromList)
import           Miso
import qualified Miso.String as MS
import           System.Random.Shuffle (shuffleM)

myBackground :: MS.MisoString
myBackground = "img/full.jpg"

myElements :: [Element]
myElements =
    [ Element "Aurora" "img/aurora.png" 182 171
    , Element "A basket" "img/basket.png" 213 354
    , Element "A blue bird" "img/blue-bird.png" 424 180
    , Element "A red bird" "img/red-bird.png" 384 229
    ]

data Element = Element
    { elementName :: MS.MisoString
    , elementImg :: MS.MisoString
    , elementX :: Int
    , elementY :: Int
    }

data Model = Model 
    { modelScore :: Int
    , modelNames :: [MS.MisoString]
    } deriving (Eq)

data Action 
    = ClickOp MS.MisoString
    | NoOp 

main :: IO ()
main = do
    randomNames <- shuffleM $ map elementName myElements
    startApp App 
        { model = Model 0 randomNames
        , update = updateModel
        , view = viewModel
        , subs = []
        , events = defaultEvents
        , initialAction = NoOp
        , mountPoint = Nothing
        }

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp theModel = noEff theModel
updateModel (ClickOp _) m@(Model _ []) = noEff m
updateModel (ClickOp clickedName) (Model score xxs@(x:xs)) =
    noEff $ if clickedName == x then Model (score+1) xs else Model (score-1) xxs

viewModel :: Model -> View Action
viewModel (Model score names) = 
    span_ []
        ([ h1_ [] [ text "piclickmiso" ]
         , img_ [ src_ myBackground
                , style_ $ fromList [("position", "relative")]
                ]
         ] ++ map fmtElement myElements ++
         [ p_ [] [ text $ if null names then "Well done" else head names ]
         , p_ [] [ text (MS.concat ["score: ", MS.ms score]) ]
         , p_ [] [ a_ [ href_ "Main.hs" ] [ "source code" ] ]
         , p_ [] [ a_ [ href_ "../index.html" ] [ "piclickfunc" ] ]
         ]
        )
    where fmtElement (Element n i x y) = 
            img_ [ src_ i 
                 , onClick (ClickOp n)
                 , style_ $ fromList 
                     [ ("position", "absolute") 
                     , ( "left", MS.concat [ MS.ms x, "px" ] )
                     , ( "top", MS.concat [ MS.ms y, "px" ] )
                     ]
                 ]

