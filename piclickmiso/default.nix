{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") { } }:

let

  _miso = pkgs.haskell.packages.ghcjs.callCabal2nix "miso" (pkgs.fetchFromGitHub {
    rev = "0.21.1.0";
    sha256 = "19x9ym4399i6ygs0hs9clgrvni0vijfg4ff3jfxgfqgjihbn0w4r";
    owner = "haskell-miso";
    repo = "miso";
  }) {};

  piclickmiso = pkgs.haskell.packages.ghcjs.callCabal2nix "piclickmiso" ./. {
    miso = _miso; 
  };

  inherit (pkgs) closurecompiler;

  piclickmiso_pkg = pkgs.runCommand "piclickmiso_pkg" { inherit piclickmiso; } ''
    mkdir -p $out
    cp -R ${piclickmiso.src}/img $out/
    cp ${piclickmiso.src}/index.html ${piclickmiso.src}/Main.hs $out/
    cd ${piclickmiso}/bin/piclickmiso.jsexe
    ${closurecompiler}/bin/closure-compiler all.js > $out/all.js
  '';

in 

if pkgs.lib.inNixShell then piclickmiso.env else piclickmiso_pkg

