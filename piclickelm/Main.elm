module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (href, src, style)
import Html.Events exposing (onClick)
import Http
import Random exposing (generate)
import Random.List exposing (shuffle)


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Element =
    { name : String
    , img : String
    , x : Int
    , y : Int
    }


myBackground : String
myBackground =
    "img/full.jpg"


myElements : List Element
myElements =
    [ { name = "Aurora", img = "img/aurora.png", x = 182, y = 171 }
    , { name = "A basket", img = "img/basket.png", x = 213, y = 354 }
    , { name = "A blue bird", img = "img/blue-bird.png", x = 424, y = 180 }
    , { name = "A red bird", img = "img/red-bird.png", x = 384, y = 229 }
    ]


myNames : List String
myNames =
    List.map (\e -> e.name) myElements


type alias Model =
    { score : Int, names : List String }


init : ( Model, Cmd Msg )
init =
    ( Model 0 [], generate MsgShuffleNames (shuffle myNames) )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type Msg
    = MsgClick String
    | MsgShuffleNames (List String)


getElement : Maybe String -> Maybe Element
getElement =
    Maybe.andThen (\name -> List.head (List.filter (\e -> e.name == name) myElements))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MsgShuffleNames shuffledNames ->
            ( { model | names = shuffledNames }, Cmd.none )

        MsgClick clickedName ->
            case model.names |> List.head |> getElement of
                Nothing ->
                    ( model, Cmd.none )

                Just element ->
                    if element.name /= clickedName then
                        ( { model | score = model.score - 1 }, Cmd.none )
                    else
                        let
                            newNames =
                                Maybe.withDefault [] (List.tail model.names)
                        in
                        ( Model (model.score + 1) newNames, Cmd.none )


view : Model -> Html Msg
view model =
    span []
        [ h1 [] [ text "piclickelm" ]
        , img [ src myBackground, style [ ( "position", "relative" ) ] ] []
        , span []
            (List.map
                (\e ->
                    img
                        [ src e.img
                        , onClick (MsgClick e.name)
                        , style
                            [ ( "position", "absolute" )
                            , ( "left", toString e.x ++ "px" )
                            , ( "top", toString e.y ++ "px" )
                            ]
                        ]
                        []
                )
                myElements
            )
        , p [] [ text (model.names |> List.head |> Maybe.withDefault "Well done") ]
        , p [] [ text ("score: " ++ toString model.score) ]
        , p [] [ a [ href "Main.elm" ] [ text "source code" ] ]
        , p [] [ a [ href "../index.html" ] [ text "piclickfunc" ] ]
        ]
