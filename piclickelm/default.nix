with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {};

stdenv.mkDerivation {

  name = "piclickelm";

  src = ./.;

  buildInputs = [ 
    elmPackages.elm-format
    elmPackages.elm-make
  ];

  buildPhase = ''
      export HOME=`pwd`
      elm-make Main.elm --yes --output all0.js
      ${pkgs.closurecompiler}/bin/closure-compiler all0.js > all.js
  '';

  installPhase = ''
      mkdir -p $out
      cp -R img $out/
      cp index.html all.js Main.elm $out/
  '';

  shellHook = ''
      alias mymake="elm-make Main.elm --yes --output all.js"
  '';

}

