#with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {};
#with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {};
with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/10b979ff213d7e63a6e072d0433a21687e311483.tar.gz") {};

stdenv.mkDerivation {

  name = "piclickpurs";

  src = ./.;

  buildInputs = [
    closurecompiler
    cacert
    git
    nodejs-8_x
    nodePackages.bower
    nodePackages.pulp
    purescript
  ];

  buildPhase = ''
      export HOME=`pwd`
      bower install
      pulp build --to all0.js
      ${pkgs.closurecompiler}/bin/closure-compiler all0.js > all.js
  '';

  installPhase = ''
      mkdir -p $out
      cp -R src $out/
      cp -R img $out/
      cp index.html all.js $out/
  '';

  shellHook = ''
      alias myclean="rm -rf bower_components node_modules .pulp-cache output all.js"
      alias myinit="bower install"
      alias mybuild="npm run build"
  '';

}

