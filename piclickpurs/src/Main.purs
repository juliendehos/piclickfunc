module Main where

import Control.Monad.Aff (Aff)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Random (RANDOM)
import CSS as CSS
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Array (head, tail)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.CSS as HC
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Prelude
import Shuffle (shuffle)

main :: Eff (HA.HalogenEffects (random :: RANDOM)) Unit
main = HA.runHalogenAff do
    body <- HA.awaitBody
    io <- runUI ui unit body
    io.query $ H.action $ QueryShuffle

ui :: forall eff. H.Component HH.HTML Query Unit Void (Aff (random :: RANDOM | eff))
ui = H.component
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }

type Element = 
    { name :: String
    , img :: String
    , x :: Number
    , y :: Number
    } 

myBackground :: String
myBackground = "img/full.jpg"

myElements :: Array Element
myElements = 
    [ { name: "Aurora", img: "img/aurora.png", x: 182.0, y: 171.0 }
    , { name: "A basket", img: "img/basket.png", x: 213.0, y: 354.0 }
    , { name: "A blue bird", img: "img/blue-bird.png", x: 424.0, y: 180.0 }
    , { name: "A red bird", img: "img/red-bird.png", x: 384.0, y: 229.0 }
    ]

type State = 
    { score :: Int
    , names :: Array String
    }

data Query a 
    = QueryClick String a 
    | QueryShuffle a

initialState :: State
initialState = 
    { score: 0
    , names: map (\ e -> e.name) myElements
    }

render :: State -> H.ComponentHTML Query
render m =
    HH.span []
        ([ HH.h1 [] [ HH.text "piclickpurs" ]
         , HH.img 
            [ HP.src myBackground 
            , HC.style do CSS.position CSS.relative
            ]
         ] <> map fmtElements myElements <>
         [ HH.p [] [ HH.text $ fromMaybe "Well done" (head m.names) ]
         , HH.p [] [ HH.text $ "score: " <> (show m.score) ]
         , HH.p [] [ HH.a [ HP.href "src/Main.purs" ] [ HH.text "source code" ] ]
         , HH.p [] [ HH.a [ HP.href "../index.html" ] [ HH.text "piclickfunc" ] ]
         ]
        )
    where fmtElements e = HH.img [ HP.src e.img
                                 , HE.onClick $ HE.input_ (QueryClick e.name)
                                 , HC.style do
                                        CSS.position CSS.absolute
                                        CSS.left $ CSS.px e.x
                                        CSS.top $ CSS.px e.y
                                 ]

eval :: forall eff. Query ~> H.ComponentDSL State Query Void (Aff (random :: RANDOM | eff))
eval (QueryClick clickedName next) = do
    m <- H.get
    case head m.names of 
        Nothing -> pure next
        Just targetName -> do
            if clickedName == targetName
                then H.put { score: m.score+1, names: fromMaybe [] (tail m.names) }
                else H.put { score: m.score-1, names: m.names }
            pure next
eval (QueryShuffle next) = do
    m <- H.get
    randomNames <- H.liftEff $ shuffle m.names
    H.put { score: m.score, names: randomNames }
    pure next

