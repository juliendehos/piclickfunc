{-# LANGUAGE OverloadedStrings #-}

import Data.Monoid ((<>))
import qualified Data.Text as T
import Prelude
import Reflex.Dom
import System.Random.Shuffle (shuffleM)

myBackground :: T.Text
myBackground = "img/full.jpg"

data Sprite = Sprite
    { spriteName :: T.Text
    , spriteImg :: T.Text
    , spriteX :: Int
    , spriteY :: Int
    }

mySprites :: [Sprite]
mySprites =
    [ Sprite "Aurora" "img/aurora.png" 182 171
    , Sprite "A basket" "img/basket.png" 213 354
    , Sprite "A blue bird" "img/blue-bird.png" 424 180
    , Sprite "A red bird" "img/red-bird.png" 384 229
    ]

data Model = Model
    { modelScore :: Int
    , modelNames :: [T.Text]
    }

main :: IO ()
main = do
    randomNames <- shuffleM $ map spriteName mySprites
    let initialModel = Model 0 randomNames
    mainWidget $ do
        el "h1" $ text "piclickreflex"
        elAttr "img" ("src" =: "img/full.jpg" <> "style" =: "position:relative") blank
        es <- mapM fmtSprite mySprites
        let esInc = map (\ (e, n) -> (if n=="Aurora" then 1 else (-1)) <$ domEvent Click e) es
        -- el "p" $ text $ head $ modelNames initialModel
        el "p" $ text $ spriteName $ head mySprites
        myScore <- foldDyn (+) (0 :: Int)  $ leftmost esInc
        el "p" $ dynText $ fmap (\ s -> "score: " <> toText s) myScore
        el "p" $ elAttr "a" ("href" =: "piclickreflex/Main.hs") $ text "source code"
        el "p" $ elAttr "a" ("href" =: "../index.html") $ text "piclickfunc"

fmtSprite :: DomBuilder t m => Sprite -> m (Element EventResult (DomBuilderSpace m) t, T.Text)
fmtSprite (Sprite n i x y) = do
    let style = "position:absolute;left:" <> toText x <> "px;top:" <> toText y <> "px"
    (e, _) <- elAttr' "img" ("src" =: i <> "style" =: style) blank
    return (e, n)

toText :: Show a => a -> T.Text
toText = T.pack . show

-- https://github.com/hansroland/reflex-dom-inbits/blob/master/tutorial.md
-- https://github.com/reflex-frp/reflex-dom/blob/develop/Quickref.md

