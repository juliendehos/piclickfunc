{ pkgs ? import <nixpkgs> {} }:

let 

  _rp_ref = "ea3c9a1536a987916502701fb6d319a880fdec96";  # 2018-04-17
  _rp = import (fetchTarball "https://github.com/reflex-frp/reflex-platform/archive/${_rp_ref}.tar.gz") {};

  _project = _rp.project ({ pkgs, ... }: {
    packages = {
      piclickreflex = ./piclickreflex;
      todomvc = ./todomvc;
    };
    shells = {
      ghcjs = [
        "piclickreflex"
        "todomvc"
      ];
    };
  });

  _pkg = pkgs.stdenv.mkDerivation rec {
    name = "piclickreflex";
    src = ./.;
    buildInputs = [ _project pkgs.closurecompiler ];
    dir_piclickreflex = "${_project.ghcjs.piclickreflex}/bin/piclickreflex.jsexe";
    dir_todomvc = "${_project.ghcjs.todomvc}/bin/todomvc.jsexe";
    buildPhase = ''
      ${pkgs.closurecompiler}/bin/closure-compiler ${dir_piclickreflex}/all.js --compilation_level=ADVANCED_OPTIMIZATIONS --jscomp_off=checkVars --externs=${dir_piclickreflex}/all.js.externs > all.js
      ${pkgs.closurecompiler}/bin/closure-compiler ${dir_todomvc}/all.js --compilation_level=ADVANCED_OPTIMIZATIONS --jscomp_off=checkVars --externs=${dir_todomvc}/all.js.externs > all_todomvc.js
    '';
    installPhase = ''
      mkdir -p $out
      cp -R img $out/
      cp index.html all.js piclickreflex/Main.hs $out/
      cp index_todomvc.html all_todomvc.js $out/
    '';
  };

in

  if pkgs.lib.inNixShell then _project.shells.ghcjs else _pkg

